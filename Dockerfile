FROM scratch
ADD rangebooking ./
EXPOSE 8080
ENTRYPOINT ["./rangebooking"]