TAG?=$(shell git rev-list HEAD --max-count=1 --abbrev-commit)
export TAG


test:
	go test ./...

build:
	CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o rangebooking .

serve: build
	./rangebooking

pack: build
	docker build -t gcr.io/lorenzswtde/rangebooking-backend:$(TAG) .

upload:
	docker push gcr.io/lorenzswtde/rangebooking-backend:$(TAG)