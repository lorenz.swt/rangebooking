package main

import (
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"encoding/json"
)

type Range struct {
	ID			string 		`json:"id,omitempty"`
	Rangename			string 		`json:"rangename,omitempty"`
}

type Rangeslot struct {
	ID			string	`json:"id,omitempty"`
	Reserved	bool	`json:"reserved"`
	Rangeid		string	`json:"-"`
}

var shootingrange []Range
var rangeslots []Rangeslot

func main() {
	shootingrange = append(shootingrange, Range{ID:"1", Rangename: "50m"})
	shootingrange = append(shootingrange, Range{ID:"2", Rangename: "25m"})
	shootingrange = append(shootingrange, Range{ID:"3", Rangename: "10m"})
	rangeslots = append(rangeslots, Rangeslot{ID: "1", Reserved:false, Rangeid:"1"})
	rangeslots = append(rangeslots, Rangeslot{ID: "2", Reserved:false, Rangeid:"1"})
	rangeslots = append(rangeslots, Rangeslot{ID: "3", Reserved:false, Rangeid:"1"})
	rangeslots = append(rangeslots, Rangeslot{ID: "4", Reserved:false, Rangeid:"1"})
	rangeslots = append(rangeslots, Rangeslot{ID: "5", Reserved:false, Rangeid:"1"})
	rangeslots = append(rangeslots, Rangeslot{ID: "6", Reserved:false, Rangeid:"1"})
	rangeslots = append(rangeslots, Rangeslot{ID: "7", Reserved:false, Rangeid:"1"})
	rangeslots = append(rangeslots, Rangeslot{ID: "8", Reserved:false, Rangeid:"1"})
	rangeslots = append(rangeslots, Rangeslot{ID: "9", Reserved:false, Rangeid:"2"})
	rangeslots = append(rangeslots, Rangeslot{ID: "10", Reserved:false, Rangeid:"2"})
	rangeslots = append(rangeslots, Rangeslot{ID: "11", Reserved:false, Rangeid:"2"})
	rangeslots = append(rangeslots, Rangeslot{ID: "12", Reserved:false, Rangeid:"2"})
	rangeslots = append(rangeslots, Rangeslot{ID: "13", Reserved:false, Rangeid:"2"})
	rangeslots = append(rangeslots, Rangeslot{ID: "14", Reserved:false, Rangeid:"2"})
	rangeslots = append(rangeslots, Rangeslot{ID: "15", Reserved:false, Rangeid:"2"})
	rangeslots = append(rangeslots, Rangeslot{ID: "16", Reserved:false, Rangeid:"2"})
	rangeslots = append(rangeslots, Rangeslot{ID: "17", Reserved:false, Rangeid:"2"})
	rangeslots = append(rangeslots, Rangeslot{ID: "18", Reserved:false, Rangeid:"2"})
	rangeslots = append(rangeslots, Rangeslot{ID: "19", Reserved:false, Rangeid:"3"})
	rangeslots = append(rangeslots, Rangeslot{ID: "20", Reserved:false, Rangeid:"3"})
	rangeslots = append(rangeslots, Rangeslot{ID: "21", Reserved:false, Rangeid:"3"})
	rangeslots = append(rangeslots, Rangeslot{ID: "22", Reserved:false, Rangeid:"3"})
	rangeslots = append(rangeslots, Rangeslot{ID: "23", Reserved:false, Rangeid:"3"})
	rangeslots = append(rangeslots, Rangeslot{ID: "24", Reserved:false, Rangeid:"3"})
	rangeslots = append(rangeslots, Rangeslot{ID: "25", Reserved:false, Rangeid:"3"})
	rangeslots = append(rangeslots, Rangeslot{ID: "26", Reserved:false, Rangeid:"3"})
	rangeslots = append(rangeslots, Rangeslot{ID: "27", Reserved:false, Rangeid:"3"})
	rangeslots = append(rangeslots, Rangeslot{ID: "28", Reserved:false, Rangeid:"3"})
	router := mux.NewRouter()
	router.HandleFunc("/", healtz)
	router.HandleFunc("/api/range", GetRanges).Methods("GET")
	router.HandleFunc("/api/range/{rangeid}", GetRange).Methods("GET")
	router.HandleFunc("/api/range/{rangeid}/rangeslots", GetRangeslots).Methods("GET")
	router.HandleFunc("/api/range/{rangeid}/rangeslot/{slotid}", ChangeReservation).Methods("PATCH")
	log.Fatal(http.ListenAndServe(":8080", router))
}
func healtz(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
}

func GetRanges(w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode(shootingrange)
}

func GetRange(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	for _, item := range shootingrange {
		if item.ID == params["rangeid"] {
			json.NewEncoder(w).Encode(item)
			return
		}
	}
	w.WriteHeader(http.StatusNotFound)
}

func GetRangeslots(w http.ResponseWriter, r *http.Request) {
	slots := []Rangeslot{}
	params := mux.Vars(r)
	for _, item := range rangeslots {
		if item.Rangeid == params["rangeid"] {
			slots = append(slots, item)
		}
	}
	if len(slots) == 0 {
		w.WriteHeader(http.StatusNotFound)
	}
	json.NewEncoder(w).Encode(slots)
}

func ChangeReservation(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	for index, item := range rangeslots {
		if item.Rangeid == params["rangeid"] && item.ID == params["slotid"] {
			rangeslots[index].Reserved = !rangeslots[index].Reserved
			w.WriteHeader(http.StatusOK)
		}
	}
	w.WriteHeader(http.StatusNotFound)
}